\part{Apêndices} 

\chapterspecial{Posfácio}{}{Claudia Amigo Pino} 

\section{Tornar"-se outro}

Enquanto escrevia este caderno, Camus tornava"-se outro. De jornalista
com recursos escassos em Argel, ele passou a ser um dos escritores mais
promissores da França. Escreveu três obras fundamentais
concomitantemente: \emph{O~estrangeiro}, \emph{O~mito de Sísifo} e
\emph{Calígula}, além de começar a sua longa reflexão sobre a
\emph{Peste}, publicada cinco anos depois. Esse período extremamente
produtivo coincidiu com experiências de vida traumáticas para toda a
Europa: a guerra e o êxodo. Em um ambiente instável, onde os meios de
comunicação eram constantemente censurados, reduzidos, fechados, o
jornalista Camus teve de errar, emigrar, adaptar"-se, tornando"-se um
estrangeiro.

E, por consequência, um escritor. Desde o início da prática dos diários,
em 1935, Camus define o que significa essa necessidade cotidiana de
escrever: a ``profunda alegria da escritura'' está ligada a uma
experiência da solidão, do ``desnudamento'' diante de si
mesmo.\footnote{``Eu sofri por ser sozinho, mas, por ter guardado meu segredo, venci o
sofrimento de ser sozinho. E~hoje não conheço maior glória que viver
sozinho e ignorado. Escrever, minha alegria profunda! Consentir ao mundo
e ao prazer --- mas somente no desnudamento. Eu não seria digno de amar
a nudez das praias se não soubesse ficar nu diante de mim mesmo.'' Ver
\versal{CAMUS}, Albert. \emph{Cadernos (1935--37)} --- \emph{Esperança do mundo.}
Editora Hedra, 2014.}  Anos depois, essa felicidade seria abordada no
\emph{Mito de Sísifo}: é preciso perceber que estamos sozinhos, que não
há nenhuma entidade que zele por nós, nenhuma resposta para a pergunta
sobre a razão de estarmos na terra e que nossa existência é absurda ---
não chega a nenhum fim --- para poder ser feliz e abraçar o mundo.

Em 1939, ano em que inicia este caderno, Camus já tinha consciência
dessa necessidade de despojamento, porém ela ainda não é uma reflexão,
mas uma imagem. No livro \emph{Núpcias}, publicado em poucos exemplares
nesse mesmo ano, um homem nu, na praia, caminha entre as árvores,
absorve o cheiro do mato, carrega a terra nos pés e pula no mar. O~salto
produz as núpcias entre terra e mar, que unem seus lábios graças à pele
nua do próprio homem, que carrega e dissolve o pó na
água.\footnote{\versal{CAMUS}, Albert. \emph{Núpcias. O~verão.} São Paulo: Círculo do Livro,
1985. p\,10}  Se ele estivesse vestido, as roupas, pesadas, o
levariam para as profundezas, e a queda não produziria essa união feliz.
Nos cadernos, vemos Camus procurando esse desnudamento, seja pelo
testemunho da solidão, seja pela reflexão sobre a condição de
estrangeiro ou pela busca de uma nova forma literária, que transmita ao
leitor essa experiência.

\section{É preciso ser dois}

Talvez essa seja a grande descoberta presente neste caderno. Entre seus
primeiros escritos e \emph{O~estrangeiro}, há uma mudança na experiência
de leitura, que passa por um desdobramento de si mesmo. Albert Camus
sempre foi dois, ou mais. Jornalista e ator. Escritor e filósofo.
Dramaturgo e crítico: ``Intelectual? Sim. E~nunca renunciar. Intelectual
= aquele que se desdobra. Isso me agrada. Eu fico contente em ser os
dois''.\footnote{Ver \versal{CAMUS}, Albert. \emph{Cadernos (1935--37) --- Esperança do mundo}.
Editora Hedra, 2014.} 

O problema é se perguntar como, na prática, esses dois podem ser
percebidos na leitura individual. No momento da escrita, a narrativa, os
fatos expostos têm um significado especial para aquele que escreve,
compõem as imagens da sua reflexão sobre a vida: juntando, dessa
maneira, o escritor e o filósofo. No entanto, o leitor não tem essa
mesma reflexão, nem, evidentemente, a mesma vida. Existe a opção de
explicar, oferecer toda a experiência de vida junto com a narrativa, mas
existe também a opção de fazer uma obra de arte talhada na experiência,
``faceta de diamante na qual o brilho interior está condensado sem estar
limitado''. Como condensar esse brilho? Escrevendo menos e procurando
contar situações nas quais o leitor seja levado a refletir sobre a vida,
adquirindo o ``saber"-viver'' que era, originalmente, do autor. É~assim
que o leitor se torna dois.

Entre o seu segundo e o terceiro caderno, Camus define o saber"-viver que
ele quer que o leitor experimente: a experiência do absurdo, ou, em
outras palavras, a consciência de que estamos sós e nossas ações só tem
um destino: a morte. Para isso, ele procura em leituras, em histórias
contadas por outros, em fatos observados na prática de jornalismo,
situações em que o leitor adquira essa experiência de vida. Por exemplo:

\begin{quote}
A mulher vive com seu marido sem entender nada. Um dia, ele fala pelo
rádio. Ela é posta atrás de um vidro e pode vê"-lo sem escutá"-lo. Ele faz
somente gestos, é tudo que ela percebe. Pela primeira vez, ela o vê no
corpo dele, como um ser físico e também como o fantoche que ele é
\textnormal{(p.\pageref{nada})}.
\end{quote}
Essa história não fará parte de \emph{O~estrangeiro}, embora uma versão
adaptada apareça numa reflexão do \emph{Mito de Sísifo}. Mas a
experiência vivida pela mulher será incorporada à leitura dessa
narrativa. O~leitor vê a personagem de Meursault, como se ele estivesse
atrás de um vidro; falta alguma coisa essencial, o som que acompanha
todas as personagens dos romances realistas: a explicação de suas ações.
Jean"-Paul Sartre já tinha atentado para esse efeito de leitura, em uma
das resenhas mais célebres do livro: as frases curtas, a ausência de
ligações, mostrariam quão absurdos são os acontecimentos cotidianos, que
aparentemente ocorrem sem um fim especial, sem sentido. Assim como a
mulher que vê o marido como nunca o tinha visto, o leitor vê a vida
desse funcionário como nunca tinha visto a sua própria vida: uma
sequência de ações que não leva a lugar algum.

Mais do que uma explicação, uma filosofia, o livro permite criar um
``sentimento'' sobre o absurdo, que faz com que essa narrativa seja,
ainda hoje, tão perturbadora. Essa é a conclusão de toda a busca formal
desse caderno: ``Os sentimentos, as imagens, multiplicam a filosofia por
dez'' \textnormal{(p.\pageref{dez})}.

\section{Percorrendo o labirinto}

Entre todas as histórias e anedotas absurdas que ele anotou durante
esses anos, sem dúvida as mais marcantes são aquelas que se relacionam à
guerra e à sua experiência como um estrangeiro. Impedido de se engajar
no exército, já que sofria de tuberculose, ele foi contratado por Pascal
Pia\footnote{Pascal Pia, pseudônimo de Pierre Durand (1903--1979), escritor,
jornalista e cronista, foi diretor de Alger Républicain e companheiro de
Camus na edição do jornal clandestino Combat!, durante a resistência.
Seu grande amigo até a ruptura em 1947, ele foi fundamental nas
negociações para a publicação das obras de Camus pela editora Gallimard
e para a configuração de sua vida profissional na França.}  para trabalhar como repórter no Alger
Républicain, jornal que pretendia representar os interesses da Frente
Popular\footnote{Coalisão de partidos de esquerda que governou a França de 1936 a 1938,
liderado pelo presidente socialista Léon Blum. Apesar de conseguir
várias conquistas trabalhistas, no final foi alvo de muita resistência
pelos setores mais conservadores. A~facilidade com que Hitler avançou e
ocupou a França esteve ligada à força desse movimento que se opôs à
Frente Popular. ``Melhor Hitler que os comunistas'' clamavam alguns
muros de Paris antes da ocupação.}  francesa na província e cujo foco era
ressaltar e descrever as diferentes formas de opressão entre classes em
Argel. As reivindicações passavam muito longe da independência: para a
Frente Popular, o inimigo não era uma nação ou outra, mas a burguesia.
Depois da ocupação alemã, o jornal é fechado, mas Camus é contratado
como redator"-chefe de uma pequena publicação frente e verso que reproduz
os comunicados oficiais com algumas intervenções, Le soir républicain. O~jornal resiste apenas até janeiro de 1940 e Camus fica novamente sem
emprego e sem a menor perspectiva de encontrá"-lo. A~solução para sua
subsistência é o refúgio em Orã, cidade da família de sua noiva,
Francine Fauré. É~o seu primeiro exílio, a sua primeira experiência como
estrangeiro e onde ele começa a esboçar os primeiros manuscritos dessa
narrativa.

Mas, durante esses anos, terá pouca possibilidade de se adaptar: ele
nunca deixará de ser estrangeiro. Pascal Pia, que tinha sido expulso da
Argélia, escreve para dizer"-lhe que tinha conseguido um cargo no jornal
Paris"-Soir. Não era exatamente o emprego ideal, a publicação estava
longe de defender as ideias da Frente Popular e a sua tarefa não seria
mais do que fazer a revisão e a diagramação final, mas ele não pensou
duas vezes. Orã lhe era insuportável: a cidade era feia, de costas para
o mar, e ele não tinha nada para fazer durante o dia inteiro além de se
angustiar pela falta de perspectivas. No dia 16 de março de 1940, deixa
sua noiva na Argélia e chega a Paris, para morar sozinho em um quarto de
hotel barato em Montmartre.

Foi esse o momento em que ele se sentiu, de forma mais intensa, na
situação de estrangeiro. Paris é cinza, as pessoas têm a cara fechada,
``com o olhar angustiado dos jogadores de um estranho cassino onde é
necessário ganhar para simplesmente sobreviver''.\footnote{Tanase, Virgil. \emph{Camus}. Paris: Gallimard, 2010. p\,123} 
Mas, graças à solidão e à chuva, ele conseguiu escrever as primeiras
versões de \emph{O~estrangeiro} e \emph{O~mito de Sísifo}. Em junho,
Paris foi ocupada, pouco depois o jornal se deslocou para Lyon e Camus
se casou com Francine, em uma cerimônia simples, sem festa nem vestido
branco. Eles não chegaram a viver nem dois meses na França e o jornal
reduziu o número de funcionários: Camus ficou mais uma vez sem emprego e
eles tiveram de voltar para Orã. Por mais que fosse a Argélia, ele sabia
que não estava voltando para casa:

\begin{quote}
Não há um só lugar que os oraneses não tenham estragado com alguma
horrenda construção que poderia arruinar qualquer paisagem. Uma cidade
que vira as costas para o mar e se constrói girando em torno dela mesma
à maneira dos caracóis. Perambula"-se nesse labirinto, procurando o mar
como o fio de Ariadne. Mas gira"-se em círculos em todas essas ruas feias
e sem graça. No final, o Minotauro devora os oraneses: é o tédio
\textnormal{(p.\pageref{horrenda})}.
\end{quote}
Seu tédio agora tinha outras razões além das horrendas construções de
cidade. Ele estava à espera de que seus manuscritos fosse aceitos, de
que algum jornal precisasse novamente de funcionários, de que sua vida
pudesse recomeçar. E, durante espera, ele foi novamente acometido por
uma crise de tuberculose, que o obrigou a um repouso absoluto. Mesmo
quando finalmente começou a receber notícias positivas (da editora, de
Pia e do seu novo projeto editorial), ele não podia mais sair da cidade,
e ficou preso nas paredes do labirinto de seu exílio.

\section{Tudo me é estrangeiro}

No seu primeiro livro publicado em Argel, \emph{O~avesso e o direito},
de 1937, Camus já havia tocado no tema do estrangeiro. O~personagem do
conto ``A morte na alma'' visita Praga sozinho, à espera de uns amigos.
Hospeda"-se em um hotel barato e vai todos os dias comer o mesmo prato no
mesmo restaurante, porque é a única palavra que ele sabe pronunciar em
tcheco. Um dia, ao sair, ele percebe que os funcionários do hotel tentam
abrir a porta do quarto ao lado. Ao voltar, descobre o que já
pressentia: o hóspede estava morto. Nesse momento, a personagem, que já
não estava confortável com a solidão do turista sem companhia, entra em
uma aberta crise:

\begin{quote}
Há dias, eu não pronunciara uma única palavra, e meu coração explodia de
gritos e de revoltas contidas. Eu teria chorado como uma criança se
alguém abrisse os braços para mim. Lá pelo fim da tarde, abatido pelo
cansaço, olhava perdidamente para o trinco da minha porta, com a cabeça
oca, repetindo uma melodia popular de acordeão. Naquele momento, eu não
conseguia mais ir adiante. Sem país, sem cidade, sem quarto e sem nome,
loucura ou conquista, humilhação ou inspiração, eu iria saber ou me
consumir? Bateram à porta e meus amigos entraram. Estava salvo, mesmo
que frustrado. Acho que eu disse: ``Fico contente em revê"-los''. Mas eu
estou certo de que aí pararam minhas confissões, e de que, a seus olhos,
fiquei sendo o homem que eles haviam deixado.\footnote{\versal{CAMUS}, Albert. \emph{O~avesso e o direito.} Trad. Valerie Rumjanek. Rio
de Janeiro: Record, 2003. p\,84} 
\end{quote}
Depois da experiência de ser um estrangeiro, ele não se considera o
mesmo: não pode voltar para casa, ter um lugar para chamar de seu. Ele
será um estrangeiro pelo resto da vida. Em Praga, sem os lugares que
conhece, sem a língua que permite se comunicar com os outros, ele
ganhará a consciência de que essa solidão faz parte da condição humana.
Estamos no mundo como estrangeiros, sem ligação com as coisas, sem
ninguém que nos acompanhe, olhando a fechadura de um quarto barato de
hotel, esperando morrer sozinhos, como o hóspede do quarto ao lado.

Mesmo se, em Orã e Paris, ele conhecesse pessoas, lugares e ---
sobretudo --- a língua, Camus viveria um processo análogo ao de seu
personagem. Ele claramente não se sentia ligado aos lugares em que
estava, o que o fez refletir se ele realmente estaria ligado a um lugar
em especial:

\begin{quote}
O que significa o despertar súbito --- nesse quarto obscuro --- com os
barulhos de uma cidade de repente estrangeira? E tudo me é estrangeiro,
tudo, sem um ser para mim, sem um só lugar onde abrigar essa mágoa. O~que faço aqui, o que significam esses gestos, esses sorrisos? Não sou
daqui --- também não sou de outro lugar. E~o mundo não é mais do que uma
paisagem desconhecida onde meu coração não encontra mais apoio.
Estrangeiro, quem pode saber o que essa palavra quer dizer \textnormal{(p.\pageref{obscuro})}.
\end{quote}
Essa palavra, que ninguém pode saber o que quer dizer, é escolhida como
título de seu texto mais importante, aquele que o tornou mundialmente
conhecido. A~personagem desse livro, Meursault, não se encontra na mesma
posição de Camus nem do homem sozinho no quarto de Praga: ele está em
Argel, o lugar onde nasceu, mora no mesmo apartamento de sempre, conhece
seus vizinhos e não tem a menor pretensão de viajar. Por que então ele é
um estrangeiro? Encontramos a explicação em um breve diálogo do livro
entre Meursault e seu patrão, que lhe pergunta se ele estaria
interessado em trabalhar em Paris. Para surpresa do patrão, ele responde
que sim, ``mas que, no fundo, tanto fazia''. Não estava interessado numa
mudança de vida, já que ``nunca se muda de vida''.\footnote{\versal{CAMUS}, Albert. \emph{O~estrangeiro}. Trad. Valerie Rumjanek. Rio de
Janeiro: Record/ Altaya, s/d. p\,45--46.} 

Meursault é um estrangeiro porque sabe que estamos sozinhos num quarto
de hotel barato, em Praga, Paris, Orã ou Argel. Ser estrangeiro faz
parte da condição do homem, que fantasia a companhia de um Deus, que
fantasia que está ligado aos outros por amor, que fantasia que existe
algum lugar ao qual iremos depois da morte. Mas Meursault não imagina
nada disso: ele é um estrangeiro no meio de estrangeiros, já que ele é o
único que sabe que todos, de fato, são estrangeiros, por mais que tentem
mudar de vida.

\section{A besta}

No labirinto das viagens de Camus nesse período, a besta escondida não é
o tédio, como em Orã, mas a guerra. Para onde ele fosse, tinha de viajar
novamente: jornais eram fechados, precisavam mudar de sede, reduziam
funcionários. Tudo por um mesmo motivo: a guerra. No início do caderno,
Camus procura por ela, mas não reconhece os sinais; sabe que faz parte
de um grande acontecimento, mas o que fazer? Por onde começar? O que
combater?

\begin{quote}
A guerra começou. Onde está a guerra? Fora das notícias em que se deve
acreditar e dos anúncios que se deve ler, onde encontrar os sinais do
absurdo evento? Ela não está nesse céu azul sobre o mar azul, nesses
cantos estridentes de cigarras, nos ciprestes das colinas. Não é esse
recente aumento de luz nas ruas de Argel.

Queremos acreditar nela. Procuramos seu rosto e ela nos recusa. Somente
o mundo é rei e seus rostos magníficos.

Ter vivido no ódio dessa besta, tê"-la diante de si e não saber
reconhecê"-la. Tão poucas coisas mudaram. Mais tarde, sem dúvida,
surgirão a lama, o sangue, a imensa repugnância. Mas por hoje provamos
que o início das guerras é parecido com os princípios da paz: o mundo e
o coração os ignoram \textnormal{(p.\pageref{ignoram})}.

\end{quote}
A maior angústia de Camus no início da guerra era a de não poder fazer,
de fato, nada. Impedido duas vezes de se engajar por causa de sua
tuberculose reincidente, várias vezes encontramos nos cadernos a revolta
por não poder lutar. Ele poderia se colocar do lado daqueles que
criticavam a guerra, que a consideravam absurda e evitável; porém
absurda, para ele, era a própria condição humana. A~guerra era apenas
uma forma de escancarar, de obrigar a todos a serem estrangeiros, a
conviver com a solidão, com a distância, com a morte. Nessa situação de
dor generalizada, para ele não havia sentido reproduzir os comunicados
oficiais, seguro em um escritório de jornal; era preciso combatê"-la:

\begin{quote}
Não querer se solidarizar é sempre vão, isso seria bobagem e crueldade
dos outros. Não se pode dizer ``Eu ignoro''. Ou a pessoa colabora, ou
combate. Nada é menos desculpável do que a guerra e a convocação do ódio
de cada nação. Mas, uma vez instalada a guerra, é vão e covarde querer
se distanciar sob o pretexto de que não se é responsável por ela. As
torres de marfim desabaram. A~complacência é proibida para si mesmo e
para os outros \textnormal{(p.\pageref{solidarizar})}.
\end{quote}
\section{A peste libertadora}

No seu segundo exílio em Orã, ele descobriu uma forma de combater:
escrever um romance sobre a guerra. Embora o livro só tenha sido
publicado em 1947, a ideia e as pesquisas iniciais em relação a esse
projeto foram desenvolvidas neste caderno, em 1941. A~cidade das
horrendas construções é acometida pela peste bubônica, que funciona como
uma alegoria para a guerra. Os portões da cidade são fechados; não se
permite a entrada nem a saída dos cidadãos, que morrem em pilhas,
sozinhos, no isolamento. Assim, a cidade de Orã transforma"-se agora no
estrangeiro: é nela que vemos escancarada a absurda condição humana.

É impressionante a forma com que Camus descreve detalhadamente as mortes
produzidas pela guerra num momento em que o mundo estava longe de saber
o que ocorria nos campos de concentração: ``Transportam os corpos nos
bondes. Comboios inteiros repletos de flores e de mortos se dispõem ao
longo do mar. Com isso os cobradores são dispensados: os viajantes não
pagam mais'' \textnormal{(p.\pageref{viajantes})}. Mas, mesmo em suas primeiras anotações sobre
a cidade, vislumbra"-se o final feliz do romance publicado cinco anos
mais tarde: em Orã, a cidade do tédio e das horrendas construções, onde
todos morrem sozinhos com chagas purulentas pelo corpo, alguma coisa
acontece, uma força para além da catástrofe:

\begin{quote}
{[}…{]} uma das terras mais fortes do mundo faz rebentar o
cenário infeliz com o qual é coberta e faz ouvir seus gritos violentos
em cada casa e por sobre todos os telhados. E~a vida que se pode levar
em Orã além do tédio é igual a essa terra. Orã demonstra que há nos
homens alguma coisa mais forte do que suas obras \textnormal{(p.\pageref{terras})}.
\end{quote}
Que coisa é essa? O que ele já anunciava nas suas primeiras anotações
sobre a guerra: ``não querer se solidarizar é sempre vão''
\textnormal{(p.\pageref{solidarizar})}. Antes da peste, cada um vivia suas próprias ilusões;
depois, todos devem enfrentar a solidão, a dor e a morte e, por
consequência, encarar a absurdidade da existência. O~padre, o médico, o
filósofo, todos se perguntam qual o sentido de todo o sofrimento e eles
reconhecem, nos outros, sua mesma revolta. No momento em que os homens
reconhecem que a Peste não é nenhum castigo divino, que ela é tão
absurda quanto a própria existência, eles veem que sua sociabilidade é o
único bem humano que eles podem opor sem mentiras à Peste ---
vitoriosamente ou não, pouco importa.\footnote{Reproduzimos aqui o argumento de Roland Barthes em ``La Peste Annales
d'une épidémie ou roman de la solitude?'' \emph{Œuvres Complètes}.
Paris: Seuil, 2002, Vol. 1. p\,543.} 

Talvez a solidariedade não vença a Peste, mas é a única arma contra ela.
Uma mensagem pacifista que não foi muito bem recebida pelo meio
intelectual: no pós"-guerra, onde os ânimos políticos se radicalizam, a
mensagem contra a violência armada não foi muito bem recebida pelos
setores mais à esquerda. Camus, aparentemente, se colocava contra a
Revolução, por isso ele foi visto por muitos como um anti"-comunista. Mas
estamos muito longe do fim da guerra e, naquele momento, mais do que
militar contra a guerra, ele tentaria transformar essa solidariedade em
uma arma de combate. Ele não batalharia nunca como soldado, jamais
carregaria um fuzil, mas ele seria redator"-chefe de um jornal
clandestino --- Combat! --- em que, a partir da análise dos fatos e das
projeções, ele criaria novas redes de sociabilidade, que reforçariam a
oposição ao regime nazista.

Ele estaria longe de seu sonho inicial de ser ator, ou mesmo romancista.
A~sua vida se voltou para essa guerra que não estava em nenhum lugar e
que agora está em toda parte. Mas como ele cita no final deste caderno:
``O que bloqueia a estrada faz construir o caminho'' \textnormal{(p.\pageref{caminho})}.

\begin{flushright}\end{flushright}
 \begin{flushright}\end{flushright}


\section{Bibliografia}

\begin{itemize}
\item
  \versal{BARTHES}, Roland. ``La Peste. Annales d'une épidémie ou roman de la
  solitude?''. \emph{Œuvres Complètes}. Paris: Seuil, 2002, Vol. 1.
\item
  \versal{CAMUS}, Albert. \emph{Núpcias, o Verão}. Trad. Vera Queiroz da Costa e
  Silva. São Paulo: Círculo do Livro, 1985.
\item
  \versal{CAMUS}, Albert. \emph{O~avesso e o direito.} Trad. Valerie Rumjanek.
  Rio de Janeiro: Record, 2003.
\item
  \versal{CAMUS}, Albert. \emph{O~estrangeiro}. Trad. Valerie Rumjanek. Rio de
  Janeiro: Record/ Altaya, s/d.
\item
  \versal{SARTRE}, Jean"-Paul. ``Explicação do estrangeiro''. Em \emph{Situações
  I.} Trad. Cristina Prado. São Paulo: Cosac Naify, 2005.
\item
  \versal{TANASE}, Virgil. \emph{Camus}. Paris: Gallimard, 2010.
\end{itemize}


